package com.example.terrae_motus_mobile;




import java.io.InputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.AsyncTask;

import org.json.JSONObject;

import android.support.v4.app.ListFragment;
import android.util.Log;


/**
 * Created by jgarcia on 7/31/14.
 */
public class OstesQuakeData {

    private static final String EarthQuakeData = "EarthQuakeData";


    private GetGeoJsonData connection;
    private String mJsonString;       // My test json string.
    private JSONObject mJsonObj;      // The jason object that is created dynamically by creating an Async call to the USGS server.

    public OstesQuakeData(){


        connection = new GetGeoJsonData();
        connection.execute("http://www.usgs.gov");


    }

    public JSONObject getJsonObject(){

       return mJsonObj;

    }

    public void getTitle(){

     /*   try {

          String str =  mJsonObj.getString("type");
          String mMetaDataString = mJsonObj.getString("metadata");
          JSONArray mFeatureObj = mJsonObj.getJSONArray("features");


          JSONObject mMetaDataObj = new JSONObject( mMetaDataString );
/*
            Log.i(EarthQuakeData,"Getting the first instance of URL: " + mMetaDataObj.getString("url")  );
            Log.i(EarthQuakeData,"Getting the first instance of Title: " + mMetaDataObj.getString("title")  );
            Log.i(EarthQuakeData,"Getting the first instance of Generated: " + mMetaDataObj.getString("generated")  );
            Log.i(EarthQuakeData,"Getting the Features Array with the magnitude of the quake: " + mFeatureObj.getJSONObject(0).getJSONObject("properties").getString("mag"));
            Log.i(EarthQuakeData,"Getting EarthQuake longitude: " + mFeatureObj.getJSONObject(1).getJSONObject("geometry").getJSONArray("coordinates").get(0));
            Log.i(EarthQuakeData,"Getting EarthQuake longitude: " + mFeatureObj.getJSONObject(1).getJSONObject("geometry").getJSONArray("coordinates").get(1));
            Log.i(EarthQuakeData,"Getting EarthQuake ID string: " + mFeatureObj.getJSONObject(1).get("id"));
            Log.i(EarthQuakeData, "Type is: " + str );
           // Log.i(EarthQuakeData, "Url is: " +  );
           */

      /*  } catch (JSONException e ){
            Log.e(EarthQuakeData,"Im blowing chunks");
            e.printStackTrace();
            e.getMessage();

        }  */



    }

    protected
    String getUrl(JSONObject json_object) {

        try {
            String url_str = json_object.getString("url");

            return (url_str);

        } catch (JSONException e) {
            Log.e(EarthQuakeData, "Error getting url: " + e.getMessage());

            return(e.getMessage());
        }


    }



                                                //Params    // Progress  // Return of result
    private class GetGeoJsonData extends AsyncTask<String,     Void,       ArrayList<EarthQuakeEvent>>{

        private static final String EarthQuakeDataHttpConnection = "EarthQuakeDataHttpConnection";
        @Override
        protected ArrayList<EarthQuakeEvent> doInBackground(String... arg){

            ArrayList<EarthQuakeEvent> earthQuakeData;
          //  String mPastHourEarthQuakes  = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.geojson";
            String mAllQuakes       = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson";

       
          earthQuakeData =  this.getGeoJsonData(mAllQuakes);
            return earthQuakeData;

        }



         // TODO -- modify this method to accept the Vector/HashMap of the EarthQuakes return from the
         // TODO -- doInBackground method.
        @Override
        protected void onPostExecute(ArrayList<EarthQuakeEvent> result){

            // Update the list here....
             Log.i(EarthQuakeDataHttpConnection, "result is: " + result.toString());
             
             
        }



                                                    /***
                                                     *
                                                     * @param url of the datasource of the JSON object.
                                                     *
                                                     * @return A json object from the datasource.
                                                     */
        public ArrayList<EarthQuakeEvent> getGeoJsonData(String url){


            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(url);
            String result = null;
            JSONObject mGeoJsonObject = null;
            ArrayList<EarthQuakeEvent> mEarthQuakeList = new ArrayList<EarthQuakeEvent>();
            HttpResponse response;

            try {
            response = mHttpClient.execute(mHttpGet);
                HttpEntity entity = response.getEntity();

                if(entity != null){

                    InputStream mInstream = entity.getContent();
                                   result = this.convertStreamToString(mInstream);

                    // We have a JSONObject from the datasource.  We need to create the SeismicEvents
                    // based on the "type" of event.
                    mGeoJsonObject = new JSONObject(result);



                    // Instantiate a EarthQuakeEvents....add to the vector.
                     for(int c = 0; c < mGeoJsonObject.getJSONArray("features").length(); c++) {

                    //    Log.i(EarthQuakeDataHttpConnection, "Features from datafeed: " +  mGeoJsonObject.getJSONArray("features").getString(c));
                        Log.i(EarthQuakeDataHttpConnection, "Features type: " +           mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("properties").getString("type"));
                        Log.i(EarthQuakeDataHttpConnection, "Feature Longitude: " +       mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(0));
                        Log.i(EarthQuakeDataHttpConnection, "Feature Latitude: " +        mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(1));  
                        // Build the Vector/HashMap of EarthQuake Objects...
                        // We then return this Vector/HashMap as a result and feed this to the onPostExecute method.

                        // Add the generated earthquake events to the vector.  We will pass this vector
                        // to the Adapter for the list view. 
                        
                         mEarthQuakeList.add( EarthQuakeEvent.newInstance (mGeoJsonObject.getJSONArray("features").getJSONObject(c) ));



                    }




                }



            } catch (Exception e) {
                Log.i(EarthQuakeDataHttpConnection, "Error with http connection-> "+ e.getMessage() );
            }

             return mEarthQuakeList; 

        }


        /***
         * @param input stream, this comes in the form of a JSON input stream that needs to be converted to
         *              String Object.
         *  @return Returns a String object.  We can do what we need with this object.
         *
         */
         private String convertStreamToString(InputStream is) {
			    /*
			     * To convert the InputStream to String we use the BufferedReader.readLine()
			     * method. We iterate until the BufferedReader returns null which means
			     * there's no more data to read. Each line will be appended to a StringBuilder
			     * and returned as String.
			     */
             BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                  StringBuilder sb = new StringBuilder();

                       String line = null;
                                try {
                                            while ((line = reader.readLine()) != null) {
                                                       sb.append(line + "\n");
                                             }
                                    } catch (IOException e) {
                                                  e.printStackTrace();
                                               Log.e(EarthQuakeDataHttpConnection, "Error while reading or building string: " + e.getMessage());
                                    } finally {
                                          try {
                                                       is.close();
                                              } catch (IOException e) {
                                                    e.printStackTrace();
                                              }
                                    }
                                          return sb.toString();
                                   }

    }
}


