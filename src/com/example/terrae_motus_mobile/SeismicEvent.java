package com.example.terrae_motus_mobile;



import android.location.Location;

public interface SeismicEvent {
	

	    // Typical Setters
	    public void setEventId(String id);              // This will be determined by the implementing class.
	    public void setEventName(String name);          // Give the event a title
	    public void setLocation(float lat, float lon ); // Latitude and Longitude of the event.
	    public void setMagnitude( float magnitude);     // The magnitude of the seismic event.
	    public void setEventTime(long time);            // Event time.
	    public void setAlert(String alertMsg);          // Alert Message
	    public void setDepth(float depth);              // Depth of Event.
	    public void setType(String type);               // This is set to the type of seismic event.  "earthquake" or "quarry" being typical values.



	    // Typical Getters

	    public String getEventId();
	    public String getEventName();
	    public Location getLocation( );
	    public float getMagnitude();
	    public long getEventTime();
	    public String getAlert();
	    public float getDepth();
	    public String getType();




	}

