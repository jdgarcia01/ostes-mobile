package com.example.terrae_motus_mobile;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;


public class OstesSettingsFragment extends Fragment {
	
	// defined for Log...
	private static final String SettingsFragment = "Terrae Motus Settings Fragment"; 
	
	private SharedPreferences mTerraePrefs; 
	private SharedPreferences.Editor mPrefEditor; 
	private int mUniqueIdOfRadioButton; 
	
	
	// private constructor, since we are not sub-classing this fragment.
	private OstesSettingsFragment(){
	
		
	}
	
	// newInstance used to create an instance of this fragment.
	public static OstesSettingsFragment newInstance(){
		
		return new OstesSettingsFragment(); 
		
	}
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		// Open the Preferences.
		mTerraePrefs = 	this.getActivity().getSharedPreferences("com.example.terrae_motus_mobile.prefs", Context.MODE_PRIVATE); 
	
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanceState){
		
		this.setRetainInstance(true);
		
		View settings_fragment = inflater.inflate(R.layout.fragment_geo_quake_settings,view, false); 
		
		RadioGroup mRadioGroup = (RadioGroup)settings_fragment.findViewById(R.id.radioGroup1);
	
		
		// Select the proper RadioButton based on the users preferences set, if not 
		// select the second radio button (events for the past week). 
		 mRadioGroup.check(mTerraePrefs.getInt("RadioId", R.id.radio1 ));
		
			
		// Listen for Radio button changes, we save the changes in our local preferences. 
		if(mRadioGroup != null){
			// Go into Edit Mode.
			
			mPrefEditor = mTerraePrefs.edit(); 
			
			mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					
					switch(checkedId) {
					   
					case R.id.radio0 :
						 // Pref info for events past hour.
						  mPrefEditor.putInt("RadioId", R.id.radio0);	
						  mPrefEditor.putInt("RadioString", R.string.all_events_past_hour);
						  mPrefEditor.putInt("radio_string", R.string.past_hour_M2_5);
						  mPrefEditor.commit();
						break; 
					case R.id.radio1: 
						// Pref info for events past day.
						 mPrefEditor.putInt("RadioId", R.id.radio1);	
						// mPrefEditor.putInt("RadioUrlName", R.string.all_events_past_day);
						 mPrefEditor.putInt("radio_string", R.string.past_day_M2_5);
						 mPrefEditor.commit();
						
						break;
						
					case R.id.radio2:
						// Pref info for events past week
						 mPrefEditor.putInt("RadioId", R.id.radio2);						
						 mPrefEditor.putInt("radio_string", R.string.past_7_days_M2_5);
						 mPrefEditor.commit();
						break;
						
					case R.id.radio3: 
						// Pref info for events past month.
						 mPrefEditor.putInt("RadioId", R.id.radio3);						
						 mPrefEditor.putInt("radio_string", R.string.past_30_days_M2_5);
						 mPrefEditor.commit();
						
						break; 
					
					case  R.id.radio4 :
						// Pref info for significant events...
					 mPrefEditor.putInt("RadioId", R.id.radio4);						
					 mPrefEditor.putInt("radio_string", R.string.past_30_days_significant);
					 mPrefEditor.commit();
					
										
					}
					
					
				
					
					
				}
			});
		
			} 
		
		return settings_fragment; 
		
		
	}
	
	

}
