/************************************************
 * This ListFragment is used to provide the 
 * listview of the seismic events.  
 * it is called by the TerraeMotusMain class.  
 * TODO Use the ViewHolder pattern to get some
 *      smooth scrolling
 ************************************************/



package com.example.terrae_motus_mobile;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class TerraeMotusListView extends ListFragment {
	
	private static final String ListView = "ListView"; 
	private SharedPreferences mListViewPrefs; 
	private GetGeoJsonData mGetGeoJsonData; 
	private int mUrlResourceId; 
	private PopupWindow mMapPopUp; 
    private String mUrlName;
    private EarthQuakeEvent mEarthQuakeEventInfo; 
    
	
	
	static public TerraeMotusListView newInstance(){
		
		return new TerraeMotusListView();
		
	}
	
	
	
	// Private constructor....we will not subclass.
	 private TerraeMotusListView(){
		
		
	}
	
	 
	// onCreate method.  Any initialization, before view creation will take place here.  
	@Override
	public void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		

    	// load a toast message, notifying the user what 
    	// data setting they are currently using. 
    	mListViewPrefs = getActivity().getSharedPreferences("com.example.terrae_motus_mobile.prefs", Context.MODE_PRIVATE); 
    	 
    	
       mUrlResourceId = mListViewPrefs.getInt("radio_string", 0);
       mUrlName = getActivity().getResources().getString(mUrlResourceId);
    
        if(mUrlName != null){
        	      
    	Toast.makeText(getActivity(), "Showing all events past " + mUrlName , Toast.LENGTH_SHORT).show();
  	
        }
			
	}
	
	
	// Just use the default ListView layout...
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
	
		this.setRetainInstance(true);
		
		
		 // Generate the EarthQuakeData and add the results to the adapter....
		 // the EarthQuakeData class contains the AsyncTask to grab the data.  
		 mGetGeoJsonData = new GetGeoJsonData();
		 
		 
	      // The URL is dynamic, based on the settings fragment user selection.  
		  // The 
		 mGetGeoJsonData.execute(mUrlName);
			
	 
		return super.onCreateView(inflater, container, savedInstanceState); 
	}  
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
			
		super.onActivityCreated(savedInstanceState);

			
	}
	
	// When the user clicks on the list item.  We 
	// need to bring up more information. 

	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
	
	 
		// Used for debuggin only....
		Log.d(ListView, "Item Clicked: " + id); 
		Log.d(ListView, "Item Position: " + position);
		mEarthQuakeEventInfo = (EarthQuakeEvent) getListAdapter().getItem(position);
		Log.d("ListView", "Event Latitude: " + mEarthQuakeEventInfo.getLatitude());
		Log.d("ListView", "event Longitude: " + mEarthQuakeEventInfo.getLongitude());
		
		
		// Create an AlertDialog. 
	    TerraeMotusDialogFragment dialog = new TerraeMotusDialogFragment();
	    this.getFragmentManager().beginTransaction().add(R.id.container,dialog).commit();
	}
	
	
	
	/*********************************************** INNER CLASS. ***********************************************/  
	// This is the AsyncTask to get the data that we need from the USGS....
	   												// Params    Progress,  PostExecute
	 private class GetGeoJsonData extends AsyncTask<String,     Integer,       ArrayList<EarthQuakeEvent> >{

		    // String used for logging purposes...
	        private static final String EarthQuakeDataHttpConnection = "EarthQuakeDataHttpConnection";
	      
	        
	        @Override
	        protected ArrayList<EarthQuakeEvent> doInBackground(String... url){
	        	
	            ArrayList<EarthQuakeEvent> earthQuakeData;        
	          earthQuakeData =  this.getGeoJsonData(url[0].toString());
	            return earthQuakeData;

	        }

	        @Override
	        protected void onPreExecute(){
	             super.onPreExecute();	
	              	
	        }
	        
	      

	         // TODO -- modify this method to accept the Vector/HashMap of the EarthQuakes return from the
	         // TODO -- doInBackground method.
	        @Override
	        protected void onPostExecute(ArrayList<EarthQuakeEvent> result){

	            // Update the list here....
	             Log.i(EarthQuakeDataHttpConnection, "result is: " + result.toString());
	             TerraeMotusSeismicAdapter mEventAdapter = new TerraeMotusSeismicAdapter(TerraeMotusListView.this.getActivity(), R.layout.fragment_row_view, result);
	             
	             setListAdapter(mEventAdapter);
	             
	        }



	                                                    /***
	                                                     *
	                                                     * @param url of the datasource of the JSON object.
	                                                     *
	                                                     * @return A json object from the datasource.
	                                                     */
	        public ArrayList<EarthQuakeEvent> getGeoJsonData(String url){


	            HttpClient mHttpClient = new DefaultHttpClient();
	            HttpGet mHttpGet = new HttpGet(url);
	            String result = null;
	            JSONObject mGeoJsonObject = null;
	            ArrayList<EarthQuakeEvent> mEarthQuakeList = new ArrayList<EarthQuakeEvent>();
	            HttpResponse response;

	            try {
	            response = mHttpClient.execute(mHttpGet);
	                HttpEntity entity = response.getEntity();

	                if(entity != null){

	                    InputStream mInstream = entity.getContent();
	                                   result = this.convertStreamToString(mInstream);

	                    // We have a JSONObject from the datasource.  We need to create the SeismicEvents
	                    // based on the "type" of event.
	                    mGeoJsonObject = new JSONObject(result);



	                    // Instantiate an EarthQuakeEvent object....add it to the ArrayList.
	                    // We then put the 
	                     for(int c = 0; c < mGeoJsonObject.getJSONArray("features").length(); c++) {
                             /******* This is here for development/debugging purposes.  Do not put into production *****/
	                        Log.d(EarthQuakeDataHttpConnection, "Features from datafeed: " + mGeoJsonObject.getJSONArray("features").getString(c));
	                        Log.d(EarthQuakeDataHttpConnection, "Features type: " +          mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("properties").getString("type"));
	                        Log.d(EarthQuakeDataHttpConnection, "Feature Longitude: " +       mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(0));
	                        Log.d(EarthQuakeDataHttpConnection, "Feature Latitude: " +     mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(1));  
	                        // Build the Vector/HashMap of EarthQuake Objects...
	                        // We then return this Vector/HashMap as a result and feed this to the onPostExecute method.

	                       // TODO -- Fix the bug when we parse the data.  We are passing in the features and the same
	                        //        Data keeps reappearing.  We are passing in the wrong data.  
	                        // Fixed -- on 08/23/2014.
	                         mEarthQuakeList.add(EarthQuakeEvent.newInstance(mGeoJsonObject.getJSONArray("features").getJSONObject(c)));



	                    }




	                }



	            } catch (Exception e) {
	                Log.e(EarthQuakeDataHttpConnection, "Error with http connection-> "+ e.getMessage() );
	            }

	            // Return the ArrayList...this goes to the postExecute method.
	             return mEarthQuakeList; 

	        }


	        /***
	         * @param input stream, this comes in the form of a JSON input stream that needs to be converted to
	         *              String Object.
	         *  @return Returns a String object.  We can do what we need with this object.
	         *
	         */
	         private String convertStreamToString(InputStream is) {
				    /*
				     * To convert the InputStream to String we use the BufferedReader.readLine()
				     * method. We iterate until the BufferedReader returns null which means
				     * there's no more data to read. Each line will be appended to a StringBuilder
				     * and returned as String.
				     */
	             BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	                  StringBuilder sb = new StringBuilder();

	                       String line = null;
	                                try {
	                                            while ((line = reader.readLine()) != null) {
	                                                       sb.append(line + "\n");
	                                             }
	                                    } catch (IOException e) {
	                                                  e.printStackTrace();
	                                               Log.e(EarthQuakeDataHttpConnection, "Error while reading or building string: " + e.getMessage());
	                                    } finally {
	                                          try {
	                                                       is.close();
	                                              } catch (IOException e) {
	                                                    e.printStackTrace();
	                                              }
	                                    }
	                                          return sb.toString();
	                                   }

	    }
	
	
	
}
