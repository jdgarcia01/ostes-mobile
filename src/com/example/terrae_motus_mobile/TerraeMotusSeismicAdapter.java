/*************************************************
 * This is our custom adapter for our event/seismic data
 * This is called by TerraeMotusListView.  
 *************************************************/

package com.example.terrae_motus_mobile;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.Date; 

public class TerraeMotusSeismicAdapter extends ArrayAdapter<EarthQuakeEvent> {
	
	private static final String TerraeMotusSeismicAdapter = "SeismicAdapter"; 
	
	private  Context context;
	private  int layoutResourceId; 
	private  ArrayList<EarthQuakeEvent> mData; 
	
	public TerraeMotusSeismicAdapter(Context context, int row_view){
		super(context, row_view); 
	}
	
	
	public TerraeMotusSeismicAdapter(Context context, int resource, ArrayList<EarthQuakeEvent> data){
		super(context, resource, data); 
		//super(context,  resource, data); 
	this.context = context;
	this.layoutResourceId = resource;
   this.mData = data;
		
		
	}
	
	
	// get the view, inflate and add the data/object info to each row.  
	@Override
	public View getView( int position, View convertView, ViewGroup parent){
		
		LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.fragment_row_view,parent, false); 
		
		
		TextView mTextView_EarthQuakeTitle = (TextView)rowView.findViewById(R.id.map_textview);
		TextView mTextView_EarthQuakeMagnitude = (TextView)rowView.findViewById(R.id.textView2);
		TextView mTextView_EarthQuakeTime = (TextView)rowView.findViewById(R.id.textView3);
		
		String type = mData.get(position).getType();
		float magnitude = mData.get(position).getMagnitude();
		String place = mData.get(position).getEventName(); 
		String color = mData.get(position).getAlert();
		long time    = mData.get(position).getEventTime();
		Date eventTime = new Date(time); 
		
		
		mTextView_EarthQuakeTitle.setText(place);
		
		     if( magnitude > 5.0 ) {
		    	mTextView_EarthQuakeMagnitude.setTextColor(Color.RED);
		    } 
		
			
		
		// Convert the float to a string...
		mTextView_EarthQuakeMagnitude.setText(String.valueOf(magnitude));
		
		mTextView_EarthQuakeTime.setText(eventTime.toString());
	
		
				
		
		
		
		return rowView; 
		
		
	}
	
	

}
