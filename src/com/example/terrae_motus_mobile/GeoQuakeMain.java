package com.example.terrae_motus_mobile;


 
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;



public class GeoQuakeMain extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_geo_quake_main);

		// new instance being created.
		if (savedInstanceState == null) {
			this.getFragmentManager().beginTransaction().add(R.id.container, new GeoQuakeMain.SplashScreenFragment().newInstance()).commit();
			// Load the preferences 	
			
			// We are creating a previous instance.
		}/* else if( savedInstanceState != null){
			this.getFragmentManager().beginTransaction().add(R.id.container, new GeoQuakeMain.SplashScreenFragment().newInstance()).commit();
		} */
	}
	
	@Override 
	public void onRestoreInstanceState(Bundle savedInstanceState){
	//super.onRestoreInstanceState(savedInstanceState);	
		
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState){
		
	//	super.onSaveInstanceState(savedInstanceState);
		
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.geo_quake_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		int id = item.getItemId();
		
		// option item to load the list of seismic events.
		if (id == R.id.action_seismic_events) {
			
			// create an instance of the list fragment. 
			
			TerraeMotusListView mListView = TerraeMotusListView.newInstance(); 
			
			this.getFragmentManager().beginTransaction().replace(R.id.container,mListView).commit(); 
			return true;
			
		}
		
		// option item to load a map of seismic events. 
		if(id == R.id.action_map_view){
			//Intent map_view_intent = new Intent(this.getApplicationContext(), TerraeMotusMap.class);
		//	this.startActivity(map_view_intent);
			
		} 
		
		// option item to manually load the data set. 
		// This is really only for debugging.  
		if(id == R.id.action_load_data){
			// load the data from the menu item.  This will facilitate 
			//a faster load time.
			EarthQuakeData data = new EarthQuakeData();
			return true;
		}
		
		if( id == R.id.action_seismic_settings){
			
			// Load the settings fragment.  
			TerraeMotusSettingsFragment mSettingsFragment = TerraeMotusSettingsFragment.newInstance();
			this.getFragmentManager().beginTransaction().replace(R.id.container, mSettingsFragment).commit();
			
			
			return true; 
		}
		
		
	
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class SplashScreenFragment extends Fragment {

		
		private SplashScreenFragment() {
			
		}
		public SplashScreenFragment newInstance(){
			return new SplashScreenFragment(); 
		}
		

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			View rootView = inflater.inflate(R.layout.fragment_splash_screen, container, false); 
			
			return rootView;
		}
	}

}
